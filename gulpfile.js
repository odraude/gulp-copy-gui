/**
 * Created by Eduardo on 28/11/16.
 */

var gulp = require('gulp');
//var copyTasks = require ('./taskJSON/copyTasks.json');
var config = require('./config.json');
var copyTasks = require(config.file);

var onError = function(err) {
    console.log(err);
}

copyTasks.forEach(function(element,index,array){
    var arrayNamesTasks = [];
    //console.log(element.name);
    element.tasks.forEach(function(element,index, array){
        arrayNamesTasks.push(element.name);
        //console.log(element.name);
        gulp.task(element.name,function(){
            gulp.src(element.src)
                .pipe(gulp.dest(element.dst));
        });
    });
    //console.log(element.name);
    //console.log(arrayNamesTasks);
    gulp.task(element.name,arrayNamesTasks);
})
/*
// Lets us type "gulp" on the command line and run all of our tasks
gulp.task('default', ['copyfilesjava', 'copyfileslayout', 'copyfilePlugin']);

//gulp.task('copy-ios', ['copyFilesSwift', 'copyFilesStoryboard'])

// Copy fonts from a module outside of our project (like Bower)
gulp.task('copyfilesjava', function() {
    gulp.src('/Users/Eduardo/Proyectos/testIonicNativaCopia/platforms/android/src/ec/com/easysoft/bancamovil/capturacheque/*')
        .pipe(gulp.dest('/Users/Eduardo/Proyectos/PluginDepositoCheque/src/android/files'));
});

gulp.task('copyfileslayout', function() {
    gulp.src('/Users/Eduardo/Proyectos/testIonicNativaCopia/platforms/android/res/layout/*')
        .pipe(gulp.dest('/Users/Eduardo/Proyectos/PluginDepositoCheque/src/android/files'));
});


gulp.task('copyfilePlugin', function() {
    gulp.src('/Users/Eduardo/Proyectos/testIonicNativaCopia/platforms/android/src/ec/com/easysoft/DepositoCheque.java')
        .pipe(gulp.dest('/Users/Eduardo/Proyectos/PluginDepositoCheque/src/android/'));
});
*/

/*gulp.task('copyFilesSwift', function() {
 gulp.src(['/Users/Eduardo/Proyectos/testIonicNativaCopia/platforms/ios/testIonicNativa/Plugins/ec.com.easysoft.depositocheque/*','!/Users/Eduardo/Proyectos/testIonicNativaCopia/platforms/ios/testIonicNativa/Plugins/ec.com.easysoft.depositocheque/*.storyboard'])
 .pipe(gulp.dest('/Users/Eduardo/Proyectos/PluginDepositoCheque/src/ios/'));
 });*/
/*
gulp.task('copyFilesSwift', function() {
    gulp.src('/Users/Eduardo/Proyectos/testIonicNativaCopia/platforms/ios/testIonicNativa/Plugins/ec.com.easysoft.depositocheque/*')
        .pipe(gulp.dest('/Users/Eduardo/Proyectos/PluginDepositoCheque/src/ios/'));
});

gulp.task('copyFilesStoryboard', function() {
    gulp.src('/Users/Eduardo/Proyectos/testIonicNativaCopia/platforms/ios/testIonicNativa/Resources/DepositoCheque.storyboard')
        .pipe(gulp.dest('/Users/Eduardo/Proyectos/PluginDepositoCheque/src/ios/'));
});

    */